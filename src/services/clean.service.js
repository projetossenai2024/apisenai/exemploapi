const connect = require('../db/connect')

async function cleanUpSchedules(){
 const currentDate = new Date();

 currentDate.setDate(currentDate.getDate() - 7) // definir a data
  const formatedDate = currentDate.toISOString().split('T')[0];

  const query = `DELETE FROM schedule WHERE dateEnd < ?`
  const values = [formatedDate];

  return new Promise((resolve, reject) => {
    connect.query( query, values, function(err, results){
        if(err){
            console.error("Erro Mysql", err);
            return reject(new Error("Erro ao limpar agendamentos"));
        }
        console.log("Agendamentos apagados");
        resolve("Agendamentos antigos apagados com sucesso!")
    });
  });

}

module.exports = cleanUpSchedules
